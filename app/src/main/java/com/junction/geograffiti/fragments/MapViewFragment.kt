package com.junction.geograffiti.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.PolygonOptions
import com.junction.geograffiti.R


class MapViewFragment: Fragment() {


    @SuppressLint("MissingPermission")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.map_layout, container, false)

        val supportMapFragment =
            childFragmentManager.findFragmentById(R.id.google_map) as SupportMapFragment

        supportMapFragment.getMapAsync { googleMap ->
            val helsinki = LatLng(60.168, 24.936)
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(helsinki, 13f));

            drawPolygon(googleMap)
            enableLocation(googleMap)
        }
        return view
    }

    private fun enableLocation(map: GoogleMap) {
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED ||
            ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            map.isMyLocationEnabled = true
            map.uiSettings.isMyLocationButtonEnabled = true
        }
    }


    private fun drawPolygon(map: GoogleMap) {
        arguments?.let {
            val figure = MapViewFragmentArgs
                .fromBundle(it)
                .figure
            val latLngs = figure.shapePoints
                .map { it.toLatLng() }

            val colorRed = ContextCompat.getColor(requireContext(), R.color.polygon_area)

            val polygon = map.addPolygon(PolygonOptions()
                .clickable(false)
                .addAll(latLngs)
                .strokeColor(colorRed)
                .fillColor(colorRed)
            )
            val colorGreen = ContextCompat.getColor(requireContext(), R.color.point_area)
            val colorTransparent = ContextCompat.getColor(requireContext(), R.color.transparent_area)

            figure.shapePoints.forEach {
                map.addCircle(
                    CircleOptions()
                        .clickable(false)
                        .strokeColor(colorRed)
                        .fillColor(colorRed)
                        .radius(30.0)
                        .center(it.toLatLng())
                )
            }

            figure.completedPoint.forEach {
                map.addCircle(
                    CircleOptions()
                        .clickable(false)
                        .strokeColor(colorGreen)
                        .fillColor(colorGreen)
                        .radius(30.0)
                        .center(it.toLatLng())
                )
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.let {
            Log.d("TESTTEST", MapViewFragmentArgs.fromBundle(it).figure.toString())
        }
    }
}