package com.junction.geograffiti.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.junction.geograffiti.R
import com.junction.geograffiti.shapes.ShapeAdapter
import com.junction.geograffiti.shapes.getShapes


class ChooseShapeFragment: Fragment() {

    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.choose_fragment, container, false)
        recyclerView = view.findViewById(R.id.shapes_rv)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val layoutManager = GridLayoutManager(view.context, 2)

        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = layoutManager

        // specify an adapter (see also next example)
        val adapter = ShapeAdapter { item ->
            val action = ChooseShapeFragmentDirections.actionChooseShapeFragmentToMapViewFragment(
                item.figure
            )
            findNavController().navigate(action)
        }
        recyclerView.adapter = adapter
        adapter.setList(getShapes())
    }
}