package com.junction.geograffiti.shapes

import com.junction.geograffiti.data.Figure
import com.junction.geograffiti.data.Point
import com.junction.geograffiti.data.ShapeView

fun getShapes(): List<ShapeView> {
    return listOf(
        ShapeView(
            3,
            "Walk a triangle to see Helsinki Cathedral, Uspenski Cathedral, and Market Square landmarks.",
            10.5f,
            "Gentle walk",
            Figure(1, "fs", listOf(
                Point(60.1695,  24.9354),
                Point( 60.1679, 24.9522),
                Point( 60.1643, 24.9525),
            ))
        ),
        ShapeView(
            4,
            "The walking path in Helsinki includes iconic landmarks like Helsinki Cathedral, Senate Square, Uspenski Cathedral, and Kauppatori Market Square.",
            12.8f,
            "Gentle walk",
            Figure(1, "fs", listOf(
                Point(60.1802,  24.9398),
                Point( 60.1802, 24.9498),
                Point( 60.1702, 24.9498),
                Point( 60.1702, 24.9398),
            ),
                listOf(
                    Point(60.1802,  24.9398),
                    Point( 60.1702, 24.9498)
                )
            )
        ),
        ShapeView(
            5,
            "The walking path in Helsinki forms a pentagon shape and includes modern and historic landmarks like the Helsinki Music Centre, Kiasma Museum, Helsinki Central Railway Station, Kamppi Chapel, and Old Church Park.",
            27.5f,
            "Adventurous Challenge",
            Figure(1, "fs", listOf(
                Point(60.1698,  24.9387),
                Point( 60.1683, 24.9521),
                Point( 60.1717, 24.9613),
                Point( 60.1751, 24.9473),
                Point( 60.1693, 24.9301),
            ))
        ),
        ShapeView(
            6,
            "Helsinki walking path: Cathedral, Senate Square, Market Square, waterfront, Allas Sea Pool.",
            21f,
            "Adventurous Challenge",
            Figure(2, "sdf", listOf(
                Point(60.1695, 24.9354),
                Point(60.1699, 24.9375),
                Point(60.1691, 24.9412),
                Point(60.1675, 24.9473),
                Point(60.1643, 24.9477),
                Point(60.1647, 24.9418)
            ))
        ),
        ShapeView(
            8,
            "The walking path in western Helsinki forms a convex octagon and includes attractions like Sibelius Monument, Hietaniemi Beach, Temppeliaukio Church, and Helsinki Olympic Stadium.",
            60.8f,
            "Extreme Expedition",
            Figure(1, "safa", listOf(
                Point(60.1872, 24.9521),
                Point(60.1863, 24.9605),
                Point(60.1807,24.9618),
                Point(60.1749, 24.9359),
                Point(60.1756, 24.9213),
                Point(60.1841, 24.9207),
                Point(60.1887, 24.9301),
                Point(60.1872, 24.9443)
            ))
        )
    )
}