package com.junction.geograffiti.shapes

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.View


class ShapeView : View {
    private var myShape: ShapePaint = ShapePaint()
    private var ratioRadius = 0.45f
    private var numberOfPoint = 3 //default

    constructor(context: Context?) : super(context) {
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        val w = width
        val h = height
        if (w == 0 || h == 0) {
            return
        }
        val x = w.toFloat() / 2.0f
        val y = h.toFloat() / 2.0f
        val radius: Float = if (w > h) {
            h * ratioRadius
        } else {
            w * ratioRadius
        }
        myShape.setPolygon(x, y, radius, numberOfPoint)
        canvas.drawPath(myShape.path, myShape.paint)
    }

    fun setShapeRadiusRatio(ratio: Float) {
        ratioRadius = ratio
    }

    fun setNumberOfPoint(pt: Int) {
        numberOfPoint = pt
    }
}
