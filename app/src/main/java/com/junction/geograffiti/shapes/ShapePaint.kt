package com.junction.geograffiti.shapes

import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import kotlin.math.cos
import kotlin.math.sin

class ShapePaint {

    val paint: Paint = Paint()
    val path: Path = Path()

    fun setCircle(x: Float, y: Float, radius: Float, dir: Path.Direction?) {
        path.reset()
        path.addCircle(x, y, radius, dir!!)
    }

    fun setPolygon(x: Float, y: Float, radius: Float, numOfPt: Int) {
        val section = 2.0 * Math.PI / numOfPt
        path.reset()
        path.moveTo((x + radius * cos(0.0)).toFloat(), (y + radius * sin(0.0)).toFloat())
        for (i in 1 until numOfPt) {
            path.lineTo(
                (x + radius * cos(section * i)).toFloat(),
                (y + radius * sin(section * i)).toFloat()
            )
        }
        path.close()
    }

    init {
        paint.color = Color.BLUE
        paint.strokeWidth = 3f
        paint.style = Paint.Style.STROKE
    }
}