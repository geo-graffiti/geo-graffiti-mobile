package com.junction.geograffiti.shapes

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.GoogleMap
import com.junction.geograffiti.R
import com.junction.geograffiti.data.ShapeView

class ShapeAdapter(
    private val onClickListener: (item: ShapeView) ->  Unit,
): RecyclerView.Adapter<ShapeAdapter.ShapeVH>() {

    private var list: List<ShapeView> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShapeVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.shape_item, parent, false)
        return ShapeVH(view)
    }

    override fun onBindViewHolder(holder: ShapeVH, position: Int) {
        holder.bind(list[position])
        holder.itemView.setOnClickListener {
            onClickListener.invoke(list[position])
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setList(data: List<ShapeView>) {
        list = data
        notifyDataSetChanged()
    }


    class ShapeVH(view: View): RecyclerView.ViewHolder(view) {

        private val canvasView = view.findViewById<com.junction.geograffiti.shapes.ShapeView>(R.id.canvas_view)
        private val textView = view.findViewById<TextView>(R.id.text)

        fun bind(item: ShapeView) {
            canvasView.setNumberOfPoint(item.numberOfPoints)
            canvasView.invalidate()
            textView.text = "${item.difficulty} - ${item.score.toInt()}/100\n\n${item.description}"
        }
    }

    interface OnItemClick {
        fun onClick(item: ShapeView)
    }
}