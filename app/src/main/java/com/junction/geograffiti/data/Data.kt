package com.junction.geograffiti.data

import com.google.android.gms.maps.model.LatLng
import java.io.Serializable

data class ShapeView(
    val numberOfPoints: Int,
    val description: String,
    val score: Float,
    val difficulty: String,
    val figure: Figure
)


data class Point(
    val latitude: Double,
    val longitude: Double,
    val attraction: String = ""
): Serializable {
    fun toLatLng(): LatLng = LatLng(latitude, longitude)
}

data class Figure(
    val id: Long,
    val name: String,
    val shapePoints: List<Point>,
    val completedPoint: List<Point> = emptyList(),
    val description: String = "",
    val perimeter: Double = 0.0,
    val score: Double = 0.0
): Serializable