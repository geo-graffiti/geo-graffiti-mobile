package com.junction.geograffiti.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiLoader {
    private const val BASE_URL = ""

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL) //Базовая часть адреса
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    val geoApi = retrofit.create(GeoApi::class.java);

}